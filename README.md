# Ansible Boilerplate

on server

```bash
sudo apt install -y python openssh-server linux-image-extra-virtual && reboot
```

on ansible machine

```bash
ansible-playbook ispconfig.yml --extra-vars "host=isp, is_vagrant=false" --tags "quotas" --skip-tags "vagrant_context"
```

## Dependencies

- an hypervisor: [supported by vagrant](https://www.vagrantup.com/docs/providers/)
- vagrant: [Download](https://www.vagrantup.com/downloads.html) or via [RubyGems](https://en.wikipedia.org/wiki/RubyGems)
  - [Vagrant Reload Provisioner plugin](https://github.com/aidanns/vagrant-reload)

    ```bash
    vagrant plugin install vagrant-reload
    ```
- ansible: [via system packet manager or pip](http://docs.ansible.com/ansible/intro_installation.html)

## Use case

### Test Playbook with vagrant

* launch vagrant machine

    ```bash
    vagrant up # ispconfig.yml applies on vagrant machine
    ```

* stop vagrant machine

    ```bash
    vagrant halt
    ```

* destroy vagrant machine

    ```bash
    vagrant destroy
    ```

* force provision vagrant machine

    ```bash
    vagrant provision
    ```

### Apply Playbook on managed hosts

* execute playbook on all servers from hosts

    ```bash
    ansible-playbook ispconfig.yml
    ```

* execute playbook on specific group

    ```bash
    ansible-playbook ispconfig.yml --extra-vars "is_vagrant=false"
    ```

* execute playbook on specific host

    ```bash
    ansible-playbook ispconfig.yml --extra-vars "host=isp, is_vagrant=false" --tags "quotas" --exclude-tags "vagrant_context"
    ```

* execute specific tag(s) from playbook on specific host

    ```bash
    ansible-playbook ispconfig.yml --extra-vars "host=isp" --tags "common,apt"
    ```

* create a vault for your secret

  ```bash
  ansible-vault create group_vars/all.yml
  ```

N.B clear vars and vault will load automatically if they are in host_vars or group_vars folder and file has for name a valid host or group name

* edit your vault

  ```bash
  ansible-vault edit group_vars/all.yml
  ```
* configure your vault to auto decrypt

in ansible.cfg add

```bash
...
vault_password_file = ./.vault_secret
...
```
where vault secret contain your password in clear (and yes you should keep this file away from the repo ;))


### Execute arbitray commands on managed hosts

* execute command on host via ansible

    ```bash
    ansible -i hosts bouscatel -a "uptime"
    ```

* execute command on group via ansible

    ```bash
    ansible -i hosts web -a "uptime"
    ```

## urls

### mailman

This defines the alias /cgi-bin/mailman/ for all Apache vhosts, which means you can access the Mailman admin interface for a list at http://<vhost>/cgi-bin/mailman/admin/<listname>, and the web page for users of a mailing list can be found at http://<vhost>/cgi-bin/mailman/listinfo/<listname>.

Under http://<vhost>/pipermail you can find the mailing list archives.

## see also
- http://docs.ansible.com/ansible/playbooks_best_practices.html#directory-layout
- https://www.vagrantup.com/docs/provisioning/ansible_common.html
